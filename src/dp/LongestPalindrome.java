package dp;

/**
 * no 5
 * 72ms 53.82%
 * 51MB 5%
 */
public class LongestPalindrome {
    public String longestPalindrome(String s) {
        char[] charArr = s.toCharArray();
        int[][] dp = new int[charArr.length][charArr.length];
        int maxi=0;
        int maxlen = 1;
        for (int i=0;i<charArr.length;i++) {
            dp[i][i] = 1;
        }
        for (int i=0;i<charArr.length-1;i++) {
            if (charArr[i] == charArr[i+1]) {
                maxi = i;
                maxlen = 2;
                dp[i][i+1] = 2;
            }
        }
        for (int step = 2; step<charArr.length; step++) {
            for (int i=0;i<charArr.length-step;i++) {
                if (charArr[i] == charArr[i+step] && dp[i+1][i+step-1] >0) {
                    dp[i][i+step] = dp[i+1][i+step-1]+2;
                    if (dp[i][i+step] > maxlen) {
                        maxlen = dp[i][i+step];
                        maxi = i;
                    }
                }
            }
        }
        return s.substring(maxi, maxi+maxlen);
    }

    public static void main(String[] args) {
//        aacabdkacaa
        System.out.println(new LongestPalindrome().longestPalindrome("aacabdkacaa"));
    }
}

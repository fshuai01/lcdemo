package str;

import java.util.ArrayList;
import java.util.List;

/**
 * no 6
 * 5ms 71.84%
 * 42.2MB 36.45%
 */
public class ZRevert {
    public String convert(String s, int numRows) {
        if (numRows == 1) {
            return s;
        }
        List<StringBuilder> rows = new ArrayList<>();
        for (int i=0;i<numRows;i++) {
            rows.add(new StringBuilder());
        }
        int direction = 1;
        int idx=0;
        for (int i = 0; i < s.length(); i++) {
            rows.get(idx).append(s.charAt(i));
            if ((idx==0 && direction == -1) || (idx == numRows-1 && direction == 1)) {
                direction*=-1;
            }
            idx += direction;
        }
        StringBuilder res = new StringBuilder();
        for (StringBuilder builder: rows) {
            res.append(builder);
        }
        return res.toString();
    }

    public static void main(String[] args) {
        System.out.println(new ZRevert().convert("PAYPALISHIRING", 3));
    }
}

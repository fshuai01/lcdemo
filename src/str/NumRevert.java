package str;

import java.util.Stack;

/**
 * no 7
 * 2ms 15.55%
 * 38.9MB 42.95%
 */
public class NumRevert {
    public int reverse(int x) {
        boolean positive = false;
        if (x > 0) {
            positive = true;
            x = -x;
        }
        int res=0;
        Stack<Integer> stack = new Stack<>();
        while (x < 0) {
            stack.push(x%10);
            x/=10;
        }
        int initSize = stack.size();
        while (!stack.isEmpty()) {
            double v = stack.pop() * Math.pow(10, initSize - stack.size() - 1);
            int temp = (int)v;
            res += temp;
        }
        // 判断溢出
        if (res >= 0 || (res==Integer.MIN_VALUE && positive)) {
            return 0;
        }
        if (positive) {
            return -res;
        }
        return res;
    }

    public static void main(String[] args) {
        // -2147483648
        System.out.println(new NumRevert().reverse(2147483647));
    }
}

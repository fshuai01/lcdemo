package str;

/**
 * 1ms 100%
 * 41.4MB 52.99%
 */
public class AtoiSolution {
    public int myAtoi(String s) {
        char[] charArr = s.toCharArray();
        int res = 0;
        boolean prefix = true;
        boolean negative = false;
        for (char ch : charArr) {
            if (ch == ' ' && prefix) {
                continue;
            }
            if (ch == '-' && prefix) {
                prefix = false;
                negative = true;
                continue;
            }
            if (ch == '+' && prefix) {
                prefix = false;
                continue;
            }
            prefix = false;
            if (ch < '0' || ch > '9') {
                return negative?-res:res;
            }
            // 判断溢出
            if (res > (Integer.MAX_VALUE-(ch-'0'))/10){
                return negative?Integer.MIN_VALUE:Integer.MAX_VALUE;
            }
            // 计算
            res = res * 10 + ch - '0';
        }
        return negative?-res:res;
    }

    public static void main(String[] args) {
        System.out.println(new AtoiSolution().myAtoi("214748364 8"));
    }
}

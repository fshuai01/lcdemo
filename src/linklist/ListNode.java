package linklist;

public class ListNode {
    int val;
    ListNode next;
    ListNode() {};
    ListNode(int val) {
        this.val = val;
    }
    ListNode(int val, ListNode next) {
        this.val = val; this.next = next;
    }

    public static ListNode genListNodeFromArr(int[] arr) {
        ListNode fakeHead = new ListNode(-1);
        ListNode cur = new ListNode(arr[0]);
        fakeHead.next = cur;
        for (int i=1;i<arr.length;i++) {
            cur.next = new ListNode(arr[i]);
            cur = cur.next;
        }
        return fakeHead.next;
    }

    public static void main(String[] args) {
        int[] arr = {1,5,2,0,8};
        ListNode res = new ListNode().genListNodeFromArr(arr);
        System.out.println(res);
    }
}

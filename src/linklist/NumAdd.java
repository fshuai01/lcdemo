package linklist;

/**
 * no 2
 */
public class NumAdd {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode fakeHead = new ListNode(-1);
        ListNode cur = new ListNode((l1.val+ l2.val)%10);
        fakeHead.next = cur;
        int c = (l1.val + l2.val)/10;
        l1 = l1.next;
        l2 = l2.next;
        while (l1 != null && l2 != null) {
            cur.next = new ListNode((l1.val+l2.val+c)%10);
            c = (l1.val+l2.val+c)/10;
            l1=l1.next;
            l2=l2.next;
            cur = cur.next;
        }
        while (l1 != null) {
            cur.next = new ListNode((l1.val+c)%10);
            c = (l1.val+c)/10;
            l1=l1.next;
            cur = cur.next;
        }
        while (l2 != null) {
            cur.next = new ListNode((l2.val+c)%10);
            c = (l2.val+c)/10;
            l2=l2.next;
            cur = cur.next;
        }
        if (c>0) {
            cur.next = new ListNode(c);
        }
        return fakeHead.next;
    }

    public static void main(String[] args) {
        ListNode l1 = ListNode.genListNodeFromArr(new int[]{2,4,3});
        ListNode l2 = ListNode.genListNodeFromArr(new int[]{5,6,4});
        ListNode res = new NumAdd().addTwoNumbers(l1, l2);
        System.out.println(res);
    }
}
